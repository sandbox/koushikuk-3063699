<?php
namespace  Drupal\restuserapi\Plugin\rest\resource;

//use Drupal\rest\Plugin\ResourceBase;
//use Drupal\rest\ResourceResponse;
/**
 * provide a custom resource
 *
 * @RestResource(
 *
 *   id ="custom resource",
 *   label=@Translation("Custom Resource"),
 *   uri_paths = {
 *   "canonical" = "/custom_rest_api/custom_resource",
 *   "https://www.drupal.org/link-relations/create" = "/custom_rest_api/user_create"
 * }
 * )
 */

 class CustomResource extends ResourceBase{
    public function get(){
      $message = ['message'=>'This is GET message in Rest Api'];
      return new ResourceResponse($message);
    }
   function post($data){

        //Create new user


     $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
     $user = \Drupal\user\Entity\User::create();

     /*$user->setPassword('password');
     $user->enforceIsNew();
     $user->setEmail('test@testmail.com');
     $user->setUserName('testuser');`
     $user->activate();*/

      $message = ['message'=> 'This is test message for POST Rest API'];
      return new ResourceResponse($message);
   }
 }